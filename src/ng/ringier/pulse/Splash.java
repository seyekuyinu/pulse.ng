package ng.ringier.pulse;

import android.app.Activity;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Bundle;

public class Splash extends Activity{
	MediaPlayer ourSound;
	
	protected void onCreate(Bundle myPulseSplashScreen){
		super.onCreate(myPulseSplashScreen);
		setContentView(R.layout.splash);
		ourSound = MediaPlayer.create(Splash.this, R.raw.pulse_launch);
		ourSound.start();
		
		Thread timer = new Thread(){
			public void run(){
				try{
					sleep(5000);
					
				}
				catch(InterruptedException e){
					e.printStackTrace();
					
				}
				finally{
					Intent openDashboard = new Intent("ng.ringier.pulse.DASHBOARD");
					startActivity(openDashboard);
					
				}
				
				
			}
			
			
			
			
		};
		timer.start();
		
	}

	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		ourSound.release();
		finish();
	}
	

}
